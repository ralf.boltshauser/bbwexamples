package ch.bbw.start;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
	
	private HashMap<String, Person> personList;
	
	public MainController() {
		personList = new HashMap<>();
		personList.put("1", new Person(1,"Yoko", "Jenkins", "lorem@vitaeeratVivamus.org"));
		personList.put("2", new Person(2,"Zelenia", "Crosby", "neque.sed.dictum@Sedcongue.com"));
		personList.put("3", new Person(3,"Ali", "Tran", "arcu@tempor.ca"));
		personList.put("4", new Person(4,"Raymond", "Gentry", "turpis@velitdui.net"));
		personList.put("5", new Person(5,"MacKenzie", "Chan", "in@ornareFuscemollis.org"));
	}

	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("personList", personList.values());
		return "index";
	}

	@GetMapping("/detail")
	public String detail(Model model, @RequestParam String id) {
		model.addAttribute("personList", personList.values());
		model.addAttribute("person", personList.get(id));
		return "detail";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("personList", personList.values());

		return "add";
	}

	@GetMapping("/edit")
	public String edit(Model model, @RequestParam String id) {
		model.addAttribute("person", personList.get(id));

		return "edit";
	}

	@GetMapping("/delete")
	public String delete(Model model, @RequestParam String id) {
		personList.remove(id);
		model.addAttribute("personList", personList.values());

		return "index";
	}

	@RequestMapping("/edit")
	public String edit(Model model,@RequestParam("email") String email, @RequestParam("firstname") String firstname, @RequestParam("lastname") String lastname,  @RequestParam("id") String id) {
		personList.replace(id, new Person(Integer.parseInt(id), firstname, lastname, email));

		System.out.println(id + ":" + email +":" + firstname +":" + lastname);
		model.addAttribute("personList", personList.values());
		return "index";
	}

	@RequestMapping("/add")
	public String add(@RequestParam("email") String email, @RequestParam("firstname") String firstname, @RequestParam("lastname") String lastname) {
		System.out.println(email + ":" + firstname + ":" + lastname);
		int id = personList.values().size();
		personList.put(String.valueOf(id), new Person(id, firstname, lastname, email));
		return "add";
	}

	@GetMapping("/personeditor")
	public String personeditor(Model model) {
		// TODO: hier Speichern implementieren
		// für die spezifische Person (id aus der URL)
		
		return "personeditor";
	}
}
