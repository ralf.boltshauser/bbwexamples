package ch.bbw.vote;

public class Vote {
    private String name;
    private int amountOfVotes;

    public Vote(String name, int amountOfVotes) {
        this.name = name;
        this.amountOfVotes = amountOfVotes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmountOfVotes() {
        return amountOfVotes;
    }

    public void setAmountOfVotes(int amountOfVotes) {
        this.amountOfVotes = amountOfVotes;
    }
}
