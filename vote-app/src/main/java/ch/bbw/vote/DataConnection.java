package ch.bbw.vote;

import java.sql.*;
import java.util.HashMap;

public class DataConnection {

    public static Connection createConnection() {
        Connection conn = null;
        try {
            conn =
                    DriverManager.getConnection("jdbc:mysql://localhost/vote-app?" +
                            "user=&password=");

            // Do something with the Connection
            return conn;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return null;
    }

    public static HashMap<String, Vote> getVotes() throws SQLException {
        Connection conn = DataConnection.createConnection();
        Statement stmt = conn.createStatement();
        HashMap<String, Vote> movieList;
        movieList = new HashMap<>();


        movieList.put("1", new Vote("Jumnanji 2: The Next Level", 0));
        movieList.put("2", new Vote("Once Upon a Time... in Hollywood", 0));
        movieList.put("3", new Vote("The Gentlemen", 0));
        movieList.put("0", new Vote("Keiner von denen", 0));



        ResultSet rs = stmt.executeQuery("SELECT * FROM vote");

        // or alternatively, if you don't know ahead of time that
        // the query will be a SELECT...

        if (stmt.execute("SELECT * FROM vote")) {
            rs = stmt.getResultSet();
        }
        while (rs.next()) {
            System.out.println(rs.getString(2));
            movieList.get(rs.getString(2)).setAmountOfVotes(movieList.get(rs.getString(2)).getAmountOfVotes() + 1);
        }


        return movieList;
    }

    public static void insert(int value) throws SQLException {
        Connection conn = DataConnection.createConnection();

        Statement stmt = conn.createStatement();
        stmt.executeUpdate("INSERT INTO vote (vote) VALUES(" + value + ")");

    }
}
