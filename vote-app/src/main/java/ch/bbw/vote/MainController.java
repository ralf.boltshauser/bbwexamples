package ch.bbw.vote;

import org.springframework.boot.autoconfigure.quartz.QuartzProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.HashMap;

@Controller
public class MainController {

    private HashMap<String, Vote> movieList;

    public MainController() throws SQLException {
        movieList = DataConnection.getVotes();
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("movieList", movieList.values());
        return "index";
    }

    @RequestMapping("/vote")
    public String add(@RequestParam("movie") int value, Model model) throws SQLException {
        if (value >= 0){

            DataConnection.insert(value);
            movieList = DataConnection.getVotes();
        }

        model.addAttribute("movieList", movieList.values());

        return "index";
    }
}
