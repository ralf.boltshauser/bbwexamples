package ch.bbw.boat;

public abstract class Boat {
	
	public abstract boolean isHighlySeaworthy();

}
