package ch.bbw.boat;

public class SailBoat extends Boat{
    private boolean hochseetauglich;

    public boolean isHochseetauglich(){
        return hochseetauglich;
    }

    public void print() {
        System.out.println(isHochseetauglich());
    }

    public SailBoat(boolean hochseetauglich) {
        this.hochseetauglich = hochseetauglich;
    }

    @Override
    public boolean isHighlySeaworthy() {
        return hochseetauglich;
    }
}
