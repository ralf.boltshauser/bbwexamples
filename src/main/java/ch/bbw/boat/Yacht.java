package ch.bbw.boat;

public class Yacht extends MotorBoat{
    private int anzahlKabinen;
    private boolean hochseetauglich;

    public boolean isHochseetauglich() {
        return hochseetauglich;
    }

    public void setHochseetauglich(boolean hochseetauglich){
        this.hochseetauglich = hochseetauglich;
    }

    public void print() {
        System.out.println(isHochseetauglich());
    }

    public Yacht(boolean hochseetauglich) {
        this.hochseetauglich = hochseetauglich;
    }

    @Override
    public boolean isHighlySeaworthy() {
        return isHochseetauglich();
    }
}
