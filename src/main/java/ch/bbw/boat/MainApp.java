package ch.bbw.boat;


import java.util.ArrayList;

/*
	Author: ...
*/
public class MainApp {
	
	
	public MainApp() {

	}
	
	public static void main(String[] args) {
		new MainApp();

		ArrayList<Boat> haven = new ArrayList<>();
		haven.add(new Yacht(false));
		haven.add(new Yacht(true));
		haven.add(new FisherBoat());
		haven.add(new SailBoat(false));
		haven.add(new SailBoat(true));

		ArrayList<MotorBoat> motorboat = new ArrayList<>();

		for (int i = 0; i < haven.size(); i++) {
			if (haven.get(i) instanceof MotorBoat){
				motorboat.add((MotorBoat) haven.get(i));
			}
		}


		for (int i = 0; i < haven.size(); i++) {
			System.out.println(i + ":" + haven.get(i).isHighlySeaworthy());
		}

		for (int i = 0; i < motorboat.size(); i++) {
			motorboat.get(i).print();
		}
	}

	
}
