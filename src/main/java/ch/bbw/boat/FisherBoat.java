package ch.bbw.boat;

public class FisherBoat extends MotorBoat {
//keine Properties im UML -> keine Properties in der Applikation!


    public boolean isHochseetauglich() {
        return false;
    }


    public void print() {
        System.out.println(isHochseetauglich());
    }

    @Override
    public boolean isHighlySeaworthy() {
        return false;
    }
}
