package ch.bbw.boats;

public abstract class Boat {
	
	public abstract boolean isHighlySeaworthy();

}
